
public class FormaGeometrica {
	private String tipo;
	private float base;
	private float altura;
	
	public FormaGeometrica(float base, float altura) {
		this.tipo = "Retângulo";
		this.base = base;
		this.altura = altura;
	}

	public String getTipo() {
		return tipo;
	}
	
	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}
	
	public float calculaArea() {
		return base*altura;
	}
	
	public float calculaPerimetro() {
		return 2*(base+altura);
	}
	
}
